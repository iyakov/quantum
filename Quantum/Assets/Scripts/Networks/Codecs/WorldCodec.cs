﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Networks.Codecs
{
    public static class WorldCodec
    {
        public static int HeaderSize = 4;
        public static int SliceCapacity = 996;
        public static int RecordSize = 4;

        public static int[][] Serialize(Dictionary<IntVector3, CubeParameters> world)
        {
            int dataIndex = HeaderSize;
            int sliceIndex = 0;
            int cubeCount = world.Count;
            int sliceCount = cubeCount / SliceCapacity + (cubeCount % SliceCapacity != 0 ? 1 : 0);

            int[][] data = new int[sliceCount][];
            for (int i = 0; i < data.GetLength(0); i++) { data[i] = new int[SliceCapacity * RecordSize + HeaderSize]; }

            int size;
            if (cubeCount % SliceCapacity == 0) { size = SliceCapacity * RecordSize + HeaderSize; }
            else { size = (cubeCount % SliceCapacity) * RecordSize + HeaderSize; }

            Debug.LogFormat("Serialize. items:{0}, slice count:{1}, slice size:{2}, last slice size:{3},",
                cubeCount, sliceCount, SliceCapacity * RecordSize + HeaderSize,
                size);

            // Содержимое
            foreach (KeyValuePair<IntVector3, CubeParameters> pointMaterialPair in world)
            {
                data[sliceIndex][dataIndex + 0] = pointMaterialPair.Key.X;
                data[sliceIndex][dataIndex + 1] = pointMaterialPair.Key.Y;
                data[sliceIndex][dataIndex + 2] = pointMaterialPair.Key.Z;
                data[sliceIndex][dataIndex + 3] = (int)pointMaterialPair.Value.Material;
                dataIndex += RecordSize;
                if (dataIndex >= SliceCapacity * RecordSize)
                {
                    data[sliceIndex][0] = SliceCapacity * RecordSize + HeaderSize; // Длина
                    data[sliceIndex][1] = 0;                                       // reserved
                    data[sliceIndex][2] = 0;                                       // reserved
                    data[sliceIndex][3] = 0;                                       // reserved

                    dataIndex = HeaderSize;
                    sliceIndex++;
                }
            }

            if (cubeCount % SliceCapacity != 0) sliceIndex += 1;

            data[sliceIndex - 1][0] = size; // Длина
            data[sliceIndex - 1][1] = 0;    // reserved
            data[sliceIndex - 1][2] = 0;    // reserved
            data[sliceIndex - 1][3] = 0;    // reserved

            return data;
        }

        public static List<DeserializedCube> Deserialize(int[] data)
        {
            // Длина
            int length = data[0];

            List<DeserializedCube> newWorld = new List<DeserializedCube>();

            // Содержимое
            for (int index = HeaderSize; index < length; index += RecordSize)
            {
                try
                {
                    IntVector3 key = new IntVector3
                    {
                        X = data[index + 0],
                        Y = data[index + 1],
                        Z = data[index + 2]
                    };
                    WorldMaterial value = (WorldMaterial)data[index + 3];

                    newWorld.Add(new DeserializedCube { Position = key, Material = value });
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    throw;
                }
            }

            return newWorld;
        }
    }

    public sealed class DeserializedCube
    {
        public IntVector3 Position { get; set; }
        public WorldMaterial Material { get; set; }
    }
}
