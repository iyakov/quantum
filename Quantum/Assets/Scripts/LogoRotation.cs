﻿using UnityEngine;

namespace Assets.Scripts
{
    public class LogoRotation : MonoBehaviour
    {
        public Vector3 Speed = new Vector3(0, 10, 0);

        void Update()
        {
            transform.Rotate(Speed * Time.deltaTime);
        }
    }
}
