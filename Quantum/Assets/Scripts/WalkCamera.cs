﻿using UnityEngine;

namespace Assets.Scripts
{
    public sealed class WalkCamera : MonoBehaviour
    {
        public Camera Camera;
        public Transform Body;
        public Rigidbody Rigidbody;
        public bool IsFreeCamera = false;
        public float HorisontalSpeed = 3f;
        public float VerticalSpeed = 3f;
        public float UpwardSpeed = 3f;
        public float RotationSpeed = 0.1f;
        public float UpForce = 300f;
        public float JumpThreshold = 0.001f;
        public float CameraDistance = 3f;

        public void Start()
        {
            WorldFactory.Setup();
            Camera.transform.position = Body.position + new Vector3(CameraDistance, 2, 0);
            Camera.transform.LookAt(Body);
        }

        public void Update()
        {
            UpdateRelativePosition();
            UpdateJump();
            UpdateLook();
        }

        private bool _isInAir;
        private void UpdateJump()
        {
            if (!_isInAir && Input.GetButton(Constants.Input.JUMP))
            {
                Rigidbody.AddForce(0, UpForce, 0);
                _isInAir = true;
            }
            else
            {
                _isInAir = Mathf.Abs(Rigidbody.velocity.y) >= JumpThreshold;
            }
        }

        private Vector3 _previousPosition;
        private Vector3 _shift;

        private void UpdateLook()
        {
            if (Input.GetMouseButton(Constants.Input.RIGHT_MOUSE_BUTTON))
            {
                _shift += 0.1f * (Input.mousePosition - _previousPosition);

                if (_shift.y < -89) { _shift.y = -89; }
                if (_shift.y > -1) { _shift.y = -1; }

                Camera.transform.position = Body.position;
                Camera.transform.rotation = Quaternion.Euler(_shift.y, _shift.x, 0);
                Camera.transform.position += Camera.transform.forward * CameraDistance;

                Camera.transform.LookAt(Body.position);
                Vector3 euler = Camera.gameObject.transform.rotation.eulerAngles;
                Body.rotation = Quaternion.Euler(0, euler.y, euler.z);
            }

            _previousPosition = Input.mousePosition;
        }

        private void UpdateRelativePosition()
        {
            transform.position += Body.right * Time.deltaTime *
                                  Input.GetAxis(Constants.Input.HORIZONTAL) * HorisontalSpeed;
            transform.position += Body.forward * Time.deltaTime *
                                  Input.GetAxis(Constants.Input.VERTICAL) * VerticalSpeed;
            if (IsFreeCamera)
            {
                transform.position += Body.up * Time.deltaTime *
                                      Input.GetAxis(Constants.Input.UPWARD) * UpwardSpeed;
            }
        }
    }
}