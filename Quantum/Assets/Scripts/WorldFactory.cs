﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Assets.Scripts.Extensions;
using Assets.Scripts.Networks.Codecs;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts
{
    public static class WorldFactory
    {
        private const int BasicWorldSize = 31;
        private const string WorldSaveFilePath = "./cubeworld.txt";

        public static readonly Dictionary<IntVector3, CubeParameters> World =
            new Dictionary<IntVector3, CubeParameters>();

        public static readonly WorldMaterial[] TheWorldMaterialLayers = {
                WorldMaterial.Stone,
                WorldMaterial.Stone,
                WorldMaterial.Stone,
                WorldMaterial.Water,
                WorldMaterial.Stone,
                WorldMaterial.Earth,
                WorldMaterial.Grass,
            };

        private static GameObject _cubeGameObject;
        private static GameObject CubeGameObject
        {
            get { return _cubeGameObject ?? (_cubeGameObject = Resources.Load<GameObject>(Constants.Resources.Prefabs.CUBE)); }
        }

        public static void Setup()
        {

            for (int y = -TheWorldMaterialLayers.Length; y < 0; y++)
            {
                for (int x = -BasicWorldSize; x < BasicWorldSize; x++)
                {
                    for (int z = -BasicWorldSize; z < BasicWorldSize; z++)
                    {
                        CreateCube(new IntVector3 { X = x, Y = y, Z = z }, TheWorldMaterialLayers[y + TheWorldMaterialLayers.Length]);
                    }
                }
            }
        }

        public static CubeParameters CreateCube(Vector3 point, WorldMaterial material)
        {
            return CreateCube(point.RoundToNearest(), material);
        }
        public static CubeParameters CreateCube(IntVector3 position, WorldMaterial material, bool shouldRecreate = false)
        {
            // check
            if (World.ContainsKey(position))
            {
                if (!shouldRecreate)
                {
                    // Debug.LogWarning("Point is buzy!");
                    return World[position];
                }
            }

            // instantiate
            GameObject cube = Object.Instantiate(CubeGameObject);
            CubeParameters parameters = cube.GetComponent<CubeParameters>();
            parameters.Material = material;
            parameters.Coordinates = position;

            // return 
            World[position] = parameters;
            return parameters;
        }

        public static bool DestroyCube(Vector3 point)
        {
            IntVector3 position = point.RoundToNearest();
            CubeParameters cube;

            if (World.TryGetValue(position, out cube))
            {
                World.Remove(position);
                Object.Destroy(cube.Self);
                return true;
            }

            return false;
        }

        public static void SaveTheWholeWorldToFile()
        {
            StringBuilder content = new StringBuilder();

            foreach (KeyValuePair<IntVector3, CubeParameters> cube in World)
            {
                CubeParameters parameters = cube.Value.GetComponent<CubeParameters>();
                content.AppendLine(parameters.Coordinates.ToString());
                content.AppendLine(((int)parameters.Material).ToString());
            }

            File.WriteAllText(WorldSaveFilePath, content.ToString());
        }

        public static void ReloadTheWholeWorldFromFile()
        {
            DestroyTheWholeWorld();

            string text = File.ReadAllText(WorldSaveFilePath);
            string[] lines = text.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < lines.Length; i += 2)
            {
                int p1Index = lines[i].IndexOf("(", StringComparison.Ordinal);
                int p2Index = lines[i].IndexOf(",", p1Index + 1, StringComparison.Ordinal);
                int p3Index = lines[i].IndexOf(",", p2Index + 1, StringComparison.Ordinal);
                int p4Index = lines[i].IndexOf(")", p3Index + 1, StringComparison.Ordinal);
                string number1 = lines[i].Substring(p1Index + 1, p2Index - p1Index - 1);
                string number2 = lines[i].Substring(p2Index + 1, p3Index - p2Index - 1);
                string number3 = lines[i].Substring(p3Index + 1, p4Index - p3Index - 1);
                int x = int.Parse(number1, NumberStyles.Any);
                int y = int.Parse(number2, NumberStyles.Any);
                int z = int.Parse(number3, NumberStyles.Any);

                IntVector3 point = new IntVector3 { X = x, Y = y, Z = z };
                WorldMaterial material = (WorldMaterial)int.Parse(lines[i + 1]);

                CreateCube(point, material);
            }
        }
        public static void Patch(List<DeserializedCube> patch)
        {
            foreach (DeserializedCube cube in patch)
            {
                CreateCube(cube.Position, cube.Material, true);
            }
        }

        public static void DestroyTheWholeWorld()
        {
            foreach (KeyValuePair<IntVector3, CubeParameters> cube in World)
            {
                Object.Destroy(cube.Value.Self);
            }

            World.Clear();
        }
    }
}