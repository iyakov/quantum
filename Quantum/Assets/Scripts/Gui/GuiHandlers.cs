﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Gui
{
    public sealed class GuiHandlers : MonoBehaviour
    {
        public NetworkView NetworkView;
        public List<GameObject> ObjectsToHide;
        public List<GameObject> ObjectsToEnable;

        public void StartQuantum()
        {
            HideObjects();
            ForceLoadServer();
            EnableObjects();
        }

        private void ForceLoadServer()
        {
            NetworkView.RPC("ForceSyncWorld", RPCMode.Server);
        }

        private void HideObjects()
        {
            foreach (GameObject obj in ObjectsToHide ?? new List<GameObject>())
            {
                obj.SetActive(false);
            }
        }

        private void EnableObjects()
        {
            foreach (GameObject obj in ObjectsToEnable ?? new List<GameObject>())
            {
                obj.SetActive(true);
            }
        }
    }
}
