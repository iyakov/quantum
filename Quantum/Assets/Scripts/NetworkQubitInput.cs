﻿using UnityEngine;

namespace Assets.Scripts
{
    public sealed class NetworkQubitInput : MonoBehaviour
    {
        public NetworkView NetworkView;
        public QubitCursor QubitCursor;
        public CubeMaterialSelection CubeMaterialSelection;

        public void Update()
        {
            if (Input.GetKeyDown(Constants.Input.FORCE_SYNC_SERVER))
            {
                Debug.Log("Asking server to synchronize the world.");
                NetworkView.RPC("ForceSyncWorld", RPCMode.Server);
            }
            else if (Input.GetKeyDown(Constants.Input.FORCE_SAVE_SERVER))
            {
                Debug.Log("Asking server to save the world.");
                NetworkView.RPC("ForceSaveWorld", RPCMode.Server);
            }
            else if (Input.GetKeyDown(Constants.Input.FORCE_LOAD_SERVER))
            {
                Debug.Log("Asking server to load the world.");
                NetworkView.RPC("ForceLoadWorld", RPCMode.Server);
            }

            if (Input.GetMouseButtonDown(Constants.Input.LEFT_MOUSE_BUTTON))
            {
                Debug.Log("Asking server to create a cube.");
                NetworkView.RPC("CreateCube", RPCMode.Server, QubitCursor.CreatePoint, (int)CubeMaterialSelection.CubeMaterial);
            }
            else if (Input.GetMouseButtonDown(Constants.Input.MIDDLE_MOUSE_BUTTON))
            {
                Debug.Log("Asking server to destroy a cube.");
                NetworkView.RPC("DestroyCube", RPCMode.Server, QubitCursor.DestroyPoint);
            }
        }
    }
}
