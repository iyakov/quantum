﻿using System.Collections;
using System.Linq;
using System.Net;
using UnityEngine;

namespace Assets.Scripts.NetworkHelpers
{
    public static class QuantumNetwork
    {
        private static string _serverPublicIp;
        private static readonly HostData HostData = new HostData
        {
            comment = "Quantum",
            gameName = "Quantum",
            gameType = "Quantum",
            guid = "quatumdomain.cloudapp.net",
            useNat = false
        };

        public static string[] GetPrivateIps()
        {
            string[] privateIps = Dns.GetHostAddresses(Dns.GetHostName()).Select(a => a.ToString()).ToArray();
            privateIps.ToList().ForEach(Debug.Log);
            return privateIps;
        }

        public static IEnumerator GetPublicIp()
        {
            WWW www = new WWW("https://api.ipify.org/?format=plain");
            while (!www.isDone)
            {
                yield return null;
            }

            string publicIp = www.text;
            Debug.Log(publicIp);

            yield return publicIp;
        }

        public static IEnumerator Connect(int serverPort, int facilitatorPort, string address = null)
        {
            Network.logLevel = NetworkLogLevel.Full;
            Network.natFacilitatorPort = facilitatorPort;

            if (string.IsNullOrEmpty(address))
            {
                for (IEnumerator network = GetPublicIp(); network.MoveNext(); _serverPublicIp = network.Current as string)
                {
                    yield return network.Current;
                }

                Network.natFacilitatorIP = _serverPublicIp;
                Network.InitializeServer(Constants.Networks.QUANTUM_SERVER_MAX_PLAYERS, serverPort, useNat: true);
            }
            else
            {
                Network.natFacilitatorIP = address;
                HostData.ip = new[] { address };
                HostData.port = serverPort;
                Network.Connect(HostData);
            }

            Debug.LogFormat("Facilitator: {0}", Network.natFacilitatorIP);
        }
    }
}
