﻿using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// Handles the current cube's material to create
    /// </summary>
    public sealed class CubeMaterialSelection : MonoBehaviour
    {
        public WorldMaterial[] Materials;

        public WorldMaterial CubeMaterial { get; set; }

        private int _selectionIndex;

        public void Start()
        {
            CubeMaterial = WorldMaterial.Grass;
        }

        public void Update()
        {
            if (Input.GetKey(KeyCode.Alpha1)) { _selectionIndex = 0; }
            else if (Input.GetKey(KeyCode.Alpha2)) { _selectionIndex = 1; }
            else if (Input.GetKey(KeyCode.Alpha3)) { _selectionIndex = 2; }
            else if (Input.GetKey(KeyCode.Alpha4)) { _selectionIndex = 3; }

            CubeMaterial = Materials[_selectionIndex];
        }
    }
}
