﻿using Assets.Scripts.Extensions;
using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// Handles user mouse cursors
    /// </summary>
    public sealed class QubitCursor : MonoBehaviour
    {
        public LayerMask HitLayers;
        public GameObject CreateCursor;
        public GameObject DestroyCursor;
        public Camera MainCamera;

        public Vector3 DestroyPoint { get; set; }
        public Vector3 CreatePoint { get; set; }

        public void Start()
        {
            Cursor.visible = false;
            CreateCursor = Instantiate(CreateCursor);
            DestroyCursor = Instantiate(DestroyCursor);
        }

        public void Update()
        {
            RaycastHit mouseHit;

            if (Physics.Raycast(MainCamera.ScreenPointToRay(Input.mousePosition), out mouseHit, HitLayers))
            {
                DestroyPoint = mouseHit.transform.position;
                CreatePoint = mouseHit.normal + DestroyPoint;

                CreateCursor.transform.position = CreatePoint.Round();
                DestroyCursor.transform.position = DestroyPoint.Round();
            }
        }
    }
}