﻿using UnityEngine;

namespace Assets.Scripts
{
    public static class Constants
    {
        public static class Networks
        {
            public const int QUANTUM_SERVER_MAX_PLAYERS = 32;
            public const int QUANTUM_FACILITATOR_PORT = 26026;
            public const int QUANTUM_SERVER_PORT = 25025;
        }
        public static class Input
        {
            public const int MIDDLE_MOUSE_BUTTON = 2;
            public const int RIGHT_MOUSE_BUTTON = 1;
            public const int LEFT_MOUSE_BUTTON = 0;

            public const KeyCode FORCE_SYNC_SERVER = KeyCode.Z;
            public const KeyCode FORCE_SAVE_SERVER = KeyCode.X;
            public const KeyCode FORCE_LOAD_SERVER = KeyCode.C;

            public const string HORIZONTAL = "Horizontal";
            public const string VERTICAL = "Vertical";
            public const string UPWARD = "Upward";
            public const string JUMP = "Jump";
        }

        public static class Resources
        {
            public static class Materials
            {
                public const string CUBE = "Materials/Cube/";
            }

            public static class Prefabs
            {
                public const string CUBE = "Prefabs/Cube";
            }
        }
    }
}
