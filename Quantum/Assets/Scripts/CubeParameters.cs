﻿using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(MeshRenderer))]
    public sealed class CubeParameters : MonoBehaviour
    {
        public MeshRenderer Renderer;
        public GameObject Self;
        public WorldMaterial Material = WorldMaterial.NotSet;
        public IntVector3 Coordinates = new IntVector3();

        public void Start()
        {
            Self = gameObject;
            Renderer = GetComponent<MeshRenderer>();
            transform.position = Coordinates.ToVector3();
            string path = Constants.Resources.Materials.CUBE + Material;
            Renderer.material = Resources.Load<Material>(path);
        }
    }
}
