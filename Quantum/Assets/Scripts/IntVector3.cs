using UnityEngine;

namespace Assets.Scripts
{
    public struct IntVector3
    {
        public int X, Y, Z;

        public Vector3 ToVector3()
        {
            return new Vector3(X, Y, Z);
        }
    }
}