﻿using System.Collections;
using Assets.Scripts.NetworkHelpers;
using Assets.Scripts.Networks.Codecs;
using UnityEngine;

namespace Assets.Scripts
{
    public sealed class Synchronization : MonoBehaviour
    {
        public string Address;
        public int ServerPort = Constants.Networks.QUANTUM_SERVER_PORT;
        public int FacilitatorPort = Constants.Networks.QUANTUM_FACILITATOR_PORT;
        public NetworkView NetworkView;

        public IEnumerator Start()
        {
            // Tests.TestWorldCodec();

            for (IEnumerator network = QuantumNetwork.Connect(ServerPort, FacilitatorPort, Address); network.MoveNext();)
            {
                yield return null;
            }
        }

        #region Server Events

        public void OnServerInitialized()
        {
            Debug.Log("Server initialized and ready");
        }

        public void OnPlayerDisconnected(NetworkPlayer player)
        {
            Debug.Log("Clean up after player " + player);
            Network.RemoveRPCs(player);
            Network.DestroyPlayerObjects(player);
        }

        public void OnPlayerConnected(NetworkPlayer player)
        {
            Debug.Log("Player connected from " + player.ipAddress + ":" + player.port);
        }

        #endregion

        #region Client Events

        public void OnConnectedToServer()
        {
            Debug.Log("Connected to server");
        }

        public void OnFailedToConnect(NetworkConnectionError error)
        {
            Debug.Log("Could not connect to server: " + error);
        }

        public void OnDisconnectedFromServer(NetworkDisconnection info)
        {
            if (Network.isServer)
            {
                Debug.Log("Local server connection disconnected");
            }
            else if (info == NetworkDisconnection.LostConnection)
            {
                Debug.Log("Lost connection to the server");

            }
            else
            {
                Debug.Log("Successfully diconnected from the server");
            }
        }

        #endregion

        #region Synchronization

        public void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
        {
            //if (stream.isWriting)
            //{
            //    Debug.Log("Write!");
            //}
            //else
            //{
            //    Debug.Log("Read!");
            //    // Длина
            //    int length = _serverState.Length;
            //    stream.Serialize(ref length);
            //    // Содержимое
            //    _serverState = Enumerable.Repeat(0, length).Aggregate(
            //        new List<int>(), IntArrayAggregator(stream), StringSelector());
            //}
        }

        #endregion

        #region SERVER RPCs

        [RPC]
        public IEnumerator ForceSyncWorld(NetworkMessageInfo info)
        {
            Debug.Log("User requsted the world: " + info.sender.ipAddress);

            int[][] data = WorldCodec.Serialize(WorldFactory.World);

            foreach (int[] slice in data)
            {
                NetworkView.RPC("PatchWorldHandler", info.sender, slice);
                yield return null;
            }
        }

        [RPC]
        public void ForceSaveWorld(NetworkMessageInfo info)
        {
            Debug.Log("Server saves the world by " + info.sender.ipAddress);
            WorldFactory.SaveTheWholeWorldToFile();
        }

        [RPC]
        public void ForceLoadWorld(NetworkMessageInfo info)
        {
            Debug.Log("Server loads the world by " + info.sender.ipAddress);
            WorldFactory.ReloadTheWholeWorldFromFile();
        }

        [RPC]
        public void CreateCube(Vector3 createPoint, int material, NetworkMessageInfo info)
        {
            Debug.Log("Server creates a cube by " + info.sender.ipAddress + " @" + createPoint + " as " + material);
            WorldFactory.CreateCube(createPoint, (WorldMaterial)material);
        }

        [RPC]
        public void DestroyCube(Vector3 destroyPoint, NetworkMessageInfo info)
        {
            Debug.Log("Server destroys a cube by " + info.sender.ipAddress + " @" + destroyPoint);
            WorldFactory.DestroyCube(destroyPoint);
        }

        #endregion

        #region CLIENT RPCs

        [RPC]
        public void SetGravityHandler(bool isOn)
        {
            Debug.Log("Server turns gravity " + (isOn ? "on" : "off"));
            Physics.gravity = isOn ? new Vector3(0, -9.81f, 0) : Vector3.zero;
        }

        [RPC]
        public void PatchWorldHandler(int[] patch)
        {
            Debug.Log("Server returns the world: " + patch[0]);
            WorldFactory.Patch(WorldCodec.Deserialize(patch));
        }

        [RPC]
        public void ResetWorldCubesHandler(NetworkMessageInfo info)
        {
            Debug.Log("Server resets the world by " + info.sender.ipAddress);
            WorldFactory.DestroyTheWholeWorld();
        }

        #endregion
    }
}
