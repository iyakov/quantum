﻿using UnityEngine;

namespace Assets.Scripts.Extensions
{
    public static class VectorExtensions
    {
        public static IntVector3 RoundToNearest(this Vector3 vector)
        {
            int x = Mathf.RoundToInt(vector.x);
            int y = Mathf.RoundToInt(vector.y);
            int z = Mathf.RoundToInt(vector.z);
            return new IntVector3 { X = x, Y = y, Z = z };
        }
        public static Vector3 Round(this Vector3 vector)
        {
            int x = Mathf.RoundToInt(vector.x);
            int y = Mathf.RoundToInt(vector.y);
            int z = Mathf.RoundToInt(vector.z);
            return new Vector3(x, y, z);
        }
    }
}
