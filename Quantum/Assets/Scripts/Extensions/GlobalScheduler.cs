﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Extensions
{
    public sealed class GlobalScheduler : MonoBehaviour
    {
        private static readonly object Sync = new object();
        private static readonly List<Action> Actions = new List<Action>();
        private static readonly GlobalScheduler Scheduler = FindObjectOfType<GlobalScheduler>();
        private static volatile bool _isDone = true;

        static GlobalScheduler()
        {
        }

        public static void Invoke(Action action)
        {
            Actions.Add(action);
        }

        public void Update()
        {
            if (_isDone)
            {
                
            }
        }
    }
}