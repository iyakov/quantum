﻿namespace Assets.Scripts
{
    public enum WorldMaterial
    {
        Selection = -1,
        NotSet = 0,
        Earth = 1,
        Water = 2,
        Stone = 3,
        Grass = 4
    }
}