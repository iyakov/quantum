﻿using System;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.Scripts.Networks.Codecs;
using UnityEngine;

namespace Assets.Test
{
    public sealed class Tests
    {
        public static void TestWorldCodec()
        {
            try
            {
                var input = new Dictionary<IntVector3, CubeParameters>
                {
                    {new IntVector3 {X = 1,Y=2,Z=3},new CubeParameters {Material = WorldMaterial.Grass}  },
                    {new IntVector3 {X = 5,Y=6,Z=7},new CubeParameters {Material = WorldMaterial.Stone}  },
                };
                var x = WorldCodec.Serialize(input);
                var output1 = WorldCodec.Deserialize(x[0]);
                var output2 = WorldCodec.Deserialize(x[1]);
                if (output1[0].Position.Z != 3 && output2[0].Material != WorldMaterial.Stone)
                {
                    Debug.LogError("[FAILED] WorldCodec test!");
                }
                else
                {
                    Debug.Log("[PASSED] WorldCodec test!");
                }
            }
            catch (Exception e)
            {
                Debug.LogError("[EXEPTION] WorldCodec test! " + e);
            }
        }
    }
}
