﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Assets.Scripts.Extensions;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts
{
    public static class WorldFactory
    {
        private static readonly Dictionary<Vector3, GameObject> Cubes =
            new Dictionary<Vector3, GameObject>();

        private static GameObject _cubeGameObject;

        public static void Setup()
        {
            _cubeGameObject = Resources.Load<GameObject>(Constants.Resources.Prefabs.CUBE);

            WorldMaterial[] materials = {
                WorldMaterial.Stone,
                WorldMaterial.Stone,
                WorldMaterial.Stone,
                WorldMaterial.Earth,
                WorldMaterial.Grass,
            };

            for (int y = -5; y < 0; y++)
            {
                for (int x = -19; x < 19; x++)
                {
                    for (int z = -19; z < 19; z++)
                    {
                        CreateCube(new Vector3(x, y, z), materials[y + 5]);
                    }
                }
            }
        }

        private static readonly System.Random Random = new System.Random();
        private static WorldMaterial GetRandomMaterial()
        {
            return (WorldMaterial)(1 + (Random.Next()) % 4);
        }

        public static GameObject CreateCube(Vector3 point, WorldMaterial material)
        {
            // check
            Vector3 position = point.RoundToNearest();
            if (Cubes.ContainsKey(position))
            {
                Debug.LogWarning("Point is buzy!");
                return Cubes[position];
            }

            // instantiate
            GameObject cube = Object.Instantiate(_cubeGameObject);
            CubeParameters parameters = cube.GetComponent<CubeParameters>();
            parameters.Material = material;
            parameters.Coordinates = position;

            // return 
            Cubes[position] = cube;
            return cube;
        }

        public static bool DestroyCube(Vector3 point)
        {
            Vector3 position = point.RoundToNearest();
            GameObject cube;

            if (Cubes.TryGetValue(position, out cube))
            {
                Cubes.Remove(position);
                Object.Destroy(cube);
                return true;
            }

            return false;
        }

        public static void Save()
        {
            StringBuilder content = new StringBuilder();

            foreach (KeyValuePair<Vector3, GameObject> cube in Cubes)
            {
                CubeParameters parameters = cube.Value.GetComponent<CubeParameters>();
                content.AppendLine(parameters.Coordinates.ToString());
                content.AppendLine(((int)parameters.Material).ToString());
            }

            File.WriteAllText("./cubeworld.txt", content.ToString());
        }

        public static void Load()
        {
            foreach (KeyValuePair<Vector3, GameObject> cube in Cubes)
            {
                Object.Destroy(cube.Value);
            }
            Cubes.Clear();

            string text = File.ReadAllText("./cubeworld.txt");
            string[] lines = text.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < lines.Length; i += 2)
            {
                int p1Index = lines[i].IndexOf("(", StringComparison.Ordinal);
                int p2Index = lines[i].IndexOf(",", p1Index + 1, StringComparison.Ordinal);
                int p3Index = lines[i].IndexOf(",", p2Index + 1, StringComparison.Ordinal);
                int p4Index = lines[i].IndexOf(")", p3Index + 1, StringComparison.Ordinal);
                string number1 = lines[i].Substring(p1Index + 1, p2Index - p1Index - 1);
                string number2 = lines[i].Substring(p2Index + 1, p3Index - p2Index - 1);
                string number3 = lines[i].Substring(p3Index + 1, p4Index - p3Index - 1);
                float x = float.Parse(number1, NumberStyles.Any);
                float y = float.Parse(number2, NumberStyles.Any);
                float z = float.Parse(number3, NumberStyles.Any);

                Vector3 point = new Vector3(x, y, z);
                WorldMaterial material = (WorldMaterial)int.Parse(lines[i + 1]);

                CreateCube(point, material);
            }
        }
    }
}