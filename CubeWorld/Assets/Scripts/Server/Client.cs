﻿using UnityEngine;

public class Client : MonoBehaviour
{
    private NetworkConnectionError _networkConnectionError;
    readonly HostData _element = new HostData
    {
        //, "10.77.58.88", "192.168.1.4", "127.0.0.1" 
        ip = new[] { "191.238.107.19"},
        port = 25025,
        gameName = "",
        gameType = "",
        comment = "",
        guid = "D5662A24-3EC1-4956-A13A-CAD6420C88C2",
        useNat = true
    };

    public void OnGUI()
    {
        if (GUILayout.Button("Connect"))
        {
            Network.natFacilitatorIP = "191.238.107.19";
            Network.natFacilitatorPort = 26026;
            _networkConnectionError = Network.Connect("191.238.107.19", 25025);
        }

        GUILayout.Label(_networkConnectionError.ToString());
    }
    void OnConnectedToServer()
    {
        Debug.Log("Connected to server");
    }
    void OnFailedToConnect(NetworkConnectionError error)
    {
        Debug.Log("Could not connect to server: " + error);
    }
    void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        if (Network.isServer)
            Debug.Log("Local server connection disconnected");
        else
            if (info == NetworkDisconnection.LostConnection)
            Debug.Log("Lost connection to the server");
        else
            Debug.Log("Successfully diconnected from the server");
    }
}
