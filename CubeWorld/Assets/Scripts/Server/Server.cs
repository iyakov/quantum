﻿using UnityEngine;

public class Server : MonoBehaviour
{
    private NetworkConnectionError _networkConnectionError;

    public void OnGUI()
    {
        GUILayout.Label("");
        GUILayout.Label("");

        if (GUILayout.Button("Start Server"))
        {
            Network.logLevel = NetworkLogLevel.Full;
            Network.natFacilitatorIP = "191.238.107.19";
            Network.natFacilitatorPort = 2301;
            bool useNat = !Network.HavePublicAddress();
            Debug.Log("NAT:" + useNat);
            _networkConnectionError = Network.InitializeServer(32, 25025, useNat);
        }
        if (GUILayout.Button("Test Server 1"))
        {
            Debug.LogFormat("TestN:{0}", Network.TestConnection());
        }
        if (GUILayout.Button("Test Server 2"))
        {
            Debug.LogFormat("NNat:{0}", Network.TestConnectionNAT());
        }

        GUILayout.Label(_networkConnectionError.ToString());
    }
    void OnServerInitialized()
    {
        Debug.Log("Server initialized and ready");
    }
    void OnPlayerDisconnected(NetworkPlayer player)
    {
        Debug.Log("Clean up after player " + player);
        Network.RemoveRPCs(player);
        Network.DestroyPlayerObjects(player);
    }
    private int playerCount = 0;
    void OnPlayerConnected(NetworkPlayer player)
    {
        Debug.Log("Player " + playerCount++ + " connected from " + player.ipAddress + ":" + player.port);
        //Network.SetSendingEnabled(player, 1, true);
        //Network.SetReceivingEnabled(player, 1, true);
    }
}
