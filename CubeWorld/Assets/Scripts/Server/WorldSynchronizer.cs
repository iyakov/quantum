﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WorldSynchronizer : MonoBehaviour
{
    private string _serverState = "";

    public NetworkView NetworkView;
    public Text Text;

    public void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        Debug.Log("Sync!");

        if (stream.isWriting)
        {
            // Длина
            int length = _serverState.Length;
            stream.Serialize(ref length);
            // Содержимое
            _serverState.ToCharArray()
                .Select(c => (int)c).ToList()
                .ForEach(i => { stream.Serialize(ref i); });
        }
        else
        {
            // Длина
            int length = _serverState.Length;
            stream.Serialize(ref length);
            // Содержимое
            _serverState = Enumerable.Repeat(0, length).Aggregate(
                new List<int>(), IntArrayAggregator(stream), StringSelector());
        }
    }

    private static Func<List<int>, string> StringSelector()
    {
        return list => new string(list.Select(i => (char)i).ToArray());
    }

    private static Func<List<int>, int, List<int>> IntArrayAggregator(BitStream stream)
    {
        return (list, integer) =>
        {
            stream.Serialize(ref integer);
            list.Add(integer);
            return list;
        };
    }

    [RPC]
    public void CreateCube()
    {
        _serverState += "cube created /n/r";
    }

    public void Start()
    {
        NetworkView.observed = this;
        NetworkView.group = 1;
        NetworkView.stateSynchronization = NetworkStateSynchronization.ReliableDeltaCompressed;
        _serverState += "server initialized /n/r";
    }

    public void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            NetworkView.RPC("CreateCube", RPCMode.Server);
        }

        Text.text = _serverState;
    }
}
