﻿namespace Assets.Scripts
{
    public static class Constants
    {
        public static class Input
        {
            public const int MIDDLE_MOUSE_BUTTON = 2;
            public const int RIGHT_MOUSE_BUTTON = 1;
            public const int LEFT_MOUSE_BUTTON = 0;

            public const string HORIZONTAL = "Horizontal";
            public const string VERTICAL = "Vertical";
            public const string UPWARD = "Upward";
            public const string JUMP = "Jump";
        }

        public static class Resources
        {
            public static class Materials
            {
                public const string CUBE = "Materials/Cube/";
            }

            public static class Prefabs
            {
                public const string CUBE = "Prefabs/Cube";
            }
        }
    }
}
