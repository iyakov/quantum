﻿using UnityEngine;

namespace Assets.Scripts.Extensions
{
    public static class VectorExtensions
    {
        public static Vector3 RoundToNearest(this Vector3 vector)
        {
            int x = (int)Mathf.RoundToInt(vector.x);
            int y = (int)Mathf.RoundToInt(vector.y);
            int z = (int)Mathf.RoundToInt(vector.z);
            return new Vector3(x, y, z);
        }
    }
}
