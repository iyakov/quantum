﻿using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(MeshRenderer))]
    public sealed class CubeParameters : MonoBehaviour
    {
        public MeshRenderer Renderer;
        public WorldMaterial Material = WorldMaterial.NotSet;
        public Vector3 Coordinates = Vector3.zero;

        public void Start()
        {
            Renderer = GetComponent<MeshRenderer>();
            string path = Constants.Resources.Materials.CUBE + Material;
            Renderer.material = Resources.Load<Material>(path);
            transform.position = Coordinates;
        }
    }
}
