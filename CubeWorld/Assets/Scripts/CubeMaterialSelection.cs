﻿using UnityEngine;

namespace Assets.Scripts.Gui
{
    public class CubeMaterialSelection : MonoBehaviour
    {
        public ClickBuilder ClickBuilder;
        public RectTransform SelectionBox;
        public Vector3 SelectionShift;
        public Vector3 SelectionPosition;
        public WorldMaterial[] Materials;

        private int _selectionIndex;

        public void Start()
        {
            ClickBuilder = GetComponent<ClickBuilder>();
        }

        public void Update()
        {
            if (Input.GetKey(KeyCode.Alpha1)) { _selectionIndex = 0; }
            else if (Input.GetKey(KeyCode.Alpha2)) { _selectionIndex = 1; }
            else if (Input.GetKey(KeyCode.Alpha3)) { _selectionIndex = 2; }
            else if (Input.GetKey(KeyCode.Alpha4)) { _selectionIndex = 3; }

            ClickBuilder.CubeMaterial = Materials[_selectionIndex];
            SelectionBox.localPosition = SelectionPosition + _selectionIndex * SelectionShift;
        }
    }
}
