﻿using Assets.Scripts.Extensions;
using UnityEngine;

namespace Assets.Scripts
{
    public sealed class ClickBuilder : MonoBehaviour
    {
        public LayerMask HitLayers;
        public WorldMaterial CubeMaterial = WorldMaterial.Grass;
        public GameObject CrossCreateGameObject;
        public GameObject CrossDestroyGameObject;
        public Camera MainCamera;

        public void Start()
        {
            Cursor.visible = false;
            CrossCreateGameObject = Instantiate(CrossCreateGameObject);
            CrossDestroyGameObject = Instantiate(CrossDestroyGameObject);
        }

        public void Update()
        {
            RaycastHit mouseHit;

            if (Physics.Raycast(MainCamera.ScreenPointToRay(Input.mousePosition), out mouseHit, HitLayers))
            {
                Vector3 destroyPoint = mouseHit.transform.position;
                Vector3 createPoint = mouseHit.normal + destroyPoint;

                UpdateCross(createPoint, destroyPoint);
                UpdateWorld(createPoint, destroyPoint);
            }
        }

        private Vector3 FindViewPoint(Vector3 center, Vector3 hit)
        {
            return center;
        }

        private void UpdateWorld(Vector3 createPoint, Vector3 destroyPoint)
        {
            if (Input.GetMouseButtonDown(Constants.Input.LEFT_MOUSE_BUTTON))
            {
                WorldFactory.CreateCube(createPoint, CubeMaterial);
            }
            if (Input.GetMouseButtonDown(Constants.Input.MIDDLE_MOUSE_BUTTON))
            {
                WorldFactory.DestroyCube(destroyPoint);
            }
            if (Input.GetKey(KeyCode.Z))
            {
                WorldFactory.Save();
            }
            if (Input.GetKey(KeyCode.X))
            {
                WorldFactory.Load();
            }
        }

        private void UpdateCross(Vector3 createPoint, Vector3 destroyPoint)
        {
            CrossCreateGameObject.transform.position = createPoint.RoundToNearest();
            CrossDestroyGameObject.transform.position = destroyPoint.RoundToNearest();
        }
    }
}