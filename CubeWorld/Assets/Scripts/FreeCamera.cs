﻿using UnityEngine;

namespace Assets.Scripts
{
    public sealed class FreeCamera : MonoBehaviour
    {
        public float HorisontalSpeed = 3;
        public float VerticalSpeed = 3;
        public float UpwardSpeed = 3;

        public void Start()
        {

        }

        public void Update()
        {
            UpdateRelativePosition();
        }

        private void UpdateRelativePosition()
        {
            transform.position += transform.right * Time.deltaTime *
                                  Input.GetAxis(Constants.Input.HORIZONTAL) * HorisontalSpeed;
            transform.position += transform.forward * Time.deltaTime *
                                  Input.GetAxis(Constants.Input.VERTICAL) * VerticalSpeed;
            transform.position += transform.up * Time.deltaTime *
                                  Input.GetAxis(Constants.Input.UPWARD) * UpwardSpeed;
        }
    }
}
